

class DiGraph:
    """
    Simple class to represent a directed graph
    """
    class Edge:
        """
        Graph will be made up of directed edges; these will have a source vertex, target vertex, and distance
        """
        def __init__(self, source, target, distance):
            """
            Construct anew edge
            :param source: the name of the source vertex
            :param target: the name of the target vertex
            :param distance: the distance between vertices
            """
            self.source = source
            self.target = target
            self.distance = distance

        def __str__(self):
            """
            Returns the Edge information in human readable format
            :return: a string containing the source vertex, target vertex, and distance between vertices
            """
            return '{source} -> {target}; distance: {distance}'.format(source=self.source,
                                                                       target=self.target,
                                                                       distance=self.distance)

    class Vertex:
        """
        The will also need Vertex records.  These will contain a label and a list of outward edges.
        """
        def __init__(self, label):
            """
            Constructs a new vertex with an empty list of out edges
            :param label: The label given to the junction
            """
            self.label = label
            self.out_edges = []

        def add_out_edge(self, edge):
            """
            Adds an outward edge, representing a road which originates from this vertex
            :param edge:
            :return:
            """
            self.out_edges.append(edge)

        def out_degree(self):
            """
            The number of roads begin from this vertex
            :return:
            """
            return len(self.out_edges)

        def __str__(self):
            """
            Print details about this vertex
            :return: A string showing the source and target vertices, and the distance between them
            """
            result = ""
            for e in self.out_edges:
                result += "{src} -> {dst} = {len}; ".format(src=e.source, dst=e.target, len=e.distance)
            return result

    def __init__(self):
        """
        Constructs a new empty DiGraph
        """
        self.edges = []
        self.vertices = {}

    def to_dot(self):
        """
        Writes the DiGraph using graphvis .dot format.  Useful for visually debugging the network / paths.
        :return: A string representing the DiGraph in dot format
        """
        result = 'digraph g {\n'
        for edge in self.edges:
            result += '\t{source} -> {target} [label="{distance}"];\n'.format(source=edge.source,
                                                                              target=edge.target,
                                                                              distance=edge.distance)
        result += '}'
        return result

    def add_edge(self, edge):
        """
        Adds an edge to the graph.  This will also add or update the vertex dictionary.
        :param edge: the edge to be added
        :return: None
        """
        self.edges.append(edge)

        if edge.source in self.vertices:
            v0 = self.vertices[edge.source]
        else:
            v0 = DiGraph.Vertex(edge.source)
            self.vertices[edge.source] = v0

        if edge.target in self.vertices:
            v1 = self.vertices[edge.target]
        else:
            v1 = DiGraph.Vertex(edge.target)
            self.vertices[edge.target] = v1

        v0.add_out_edge(edge)
        #print("Graph:\n{g}".format(g=self))

    def __str__(self):
        """
        Returns the DiGraph in human readable form, useful for debugging
        :return: a string representing the list of edges and vertices
        """
        result = ""
        for (k, v) in self.vertices.items():
            neighbours = ""
            for e in v.out_edges:
                neighbours += "{source} -> {target} = {distance} ".format(source=e.source,
                                                                          target=e.target,
                                                                          distance=e.distance)
            result += "{label}: ({out_degree}) {neighbours}\n".format(label=k,
                                                                    out_degree=v.out_degree(),
                                                                    neighbours=neighbours)
        return result
