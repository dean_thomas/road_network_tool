import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="road_network",
    version="0.0.1",
    author="Dean Thomas",
    author_email="dean.plymouth@googlemail.com",
    description="A package containing graph graph and algorithms",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/dean_thomas/road_network_tool",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)