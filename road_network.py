import sys
from graph.digraph import *
from algorithms.dijsktra import *
from algorithms.prim import *


def parse_line(line):
    """
    Parses a single line from a network definition file
    :param line: the line to be parsed.  Should be in the form 'source target distance'
    :return: a new DiGraph.Edge; if the parse was unsuccessful None will be returned
    """
    tokens = line.split(' ')
    if len(tokens) == 3:
        try:
            return DiGraph.Edge(source=tokens[0], target=tokens[1], distance=float(tokens[2]))
        except ValueError:
            print('Unable to parse distance: {distance}'.format(distance=tokens[2]))
            return None
    else:
        print('Unable to parse line {line}'.format(line=line))
        return None


def load_data(filename):
    """
    Loads data from a network file and returns a new DiGraph object
    :param filename: The file to be loaded
    :return: A new DiGraph; if the file cannot be loaded this function will return None
    """
    try:
        network = DiGraph()

        with open(filename) as file:
            for line in file:
                edge = parse_line(line)
                if edge is not None:
                    network.add_edge(edge)

        return network
    except FileNotFoundError:
        print('Error: file "{:}" not found.'.format(filename))
        return None


def print_distance(digraph, distance):
    """
    Helper function for listing the currently known distances from the source
    :param digraph: the network graph
    :param distance: a dictionary of distances from the source, keyed by name
    """
    print()
    for vertex in digraph.vertices:
        print('Distance to vertex {v} is {d}'.format(v=vertex, d=distance[vertex]))


def main(args):
    """
    Main program entry point
    :param args: array of string representing the program arguments in the following format
    'road_network.py <network-filename> <origin> <destination>'
    """
    print('Args: {:}'.format(args))
    network = load_data(sys.argv[1])
    # print(network.to_dot())

    if network is not None:
        #   Extract and validate source and target
        source = sys.argv[2]
        destination = sys.argv[3]
        if source not in network.vertices:
            print('The vertex "{source}" was not found in the network.'.format(source=source))
            return
        if destination not in network.vertices:
            print('The vertex "{destination}" was not found in the network.'.format(destination=destination))
            return

        #   Compute the path
        path = dijkstra(network, source, destination)
        print(path)

        path = prim(network, source, destination)
        print(path)

        if not path == []:
            print(path)
        else:
            print("No path")


if __name__ == '__main__':
    main(sys.argv)