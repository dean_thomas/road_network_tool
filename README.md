# Road Network tool
## Find the shortest path between road junctions

Requirements:

- Built using Python 3.6
- No external dependencies
- Testing via 'unittest'

Algorithm:

- [Dijkstra's Algorithm](https://en.wikipedia.org/wiki/Dijkstra's_algorithm)

Usage:

- python road_network.py *network file* *start* *end*

Example:

- python road_network.py *exmouth-links.dat* *J1053* *J1037*

Testing:

- some unit tests are included to test the basic functionality of the script.
I was unable to find any impossible solutions in the given network.

- the unit tests use the 'unittest' testing framework module

Packaging:

- The files in this repo can be built and added to a locally hosted package server using the following command [1]:
```bash
python3 setup.py sdist register -r local upload -r local
```
  

Notes:

- it is possible to view the network created by parsing the input by uncommenting 
line 260 `print(network.to_dot())`
 
- this will output the network in graphvis format (.dot)  

- this can be viewed using a [JavaScript dot file viewer](https://jsfiddle.net/nullptr83/m3r0pcp6/) I made

[1] https://pypi.org/project/pypiserver/#upload-with-twine