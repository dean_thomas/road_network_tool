import unittest
from road_network import *


class TestRoadNetwork(unittest.TestCase):
    def setUp(self):
        self.network = load_data('../exmouth-links.dat')
        self.assertTrue(self.network is not None)

    def test1(self):
        self.assertEqual(dijkstra(self.network, 'J1053', 'J1037'), ['J1053', 'J1035', 'J1036', 'J1037'])

    def test2(self):
        self.assertEqual(dijkstra(self.network, 'J1037', 'J1053'), ['J1037', 'J1036', 'J1035', 'J1053'])

    def test3(self):
        self.assertEqual(dijkstra(self.network, 'J1053', 'J1008'),
                         ['J1053', 'J1052', 'J1040', 'J1041', 'J1042', 'J1048', 'J1047',
                          'J1043', 'J1018', 'J1026', 'J1001', 'J1031', 'J1022', 'J1008'])

    def test4(self):
        self.assertEqual(dijkstra(self.network, 'J1020', 'J1008'),
                         ['J1020', 'J1019', 'J1002', 'J1001', 'J1031', 'J1022', 'J1008'])

    def test5(self):
        self.assertEqual(dijkstra(self.network, 'J1001', 'J1008'), ['J1001', 'J1031', 'J1022', 'J1008'])

    def test6(self):
        self.assertEqual(dijkstra(self.network, 'J1021', 'X1015'), ['J1021', 'J1003', 'J1012', 'J1014', 'X1015'])

    def test7(self):
        self.assertEqual(dijkstra(self.network, 'X1024', 'X1015'),
                         ['X1024', 'J1022', 'J1031', 'J1001', 'J1002', 'J1030', 'J1003', 'J1012', 'J1014', 'X1015'])

    def test8(self):
        self.assertEqual(dijkstra(self.network, 'X1058', 'J1002'),
                         ['X1058', 'J1040', 'J1041', 'J1042', 'J1048', 'J1047', 'J1043', 'J1018', 'J1026', 'J1025',
                          'J1019', 'J1002'])


if __name__ == '__main__':
    unittest.main()