import math
from builtins import type
from typing import *
from graph.digraph import DiGraph
from algorithms.util import extract_path

def prim(digraph: DiGraph, start: str, end: str) -> DiGraph:
    C = {}
    E = {}
    Q = []
    parent = {}

    for vertex in digraph.vertices:
        assert isinstance(vertex, str)
        C[vertex] = math.inf
        E[vertex] = None
        parent[vertex] = None
        Q.append(vertex)

    C[start] = 0
    F = DiGraph()

    while len(Q) > 0:
        v = min(Q, key=lambda x: C[x])
        assert isinstance(v, str)
        Q.remove(v)

        F.vertices[v] = v
        e = E[v]
        if e is not None:
            assert isinstance(e, DiGraph.Edge)
            F.vertices[e.source] = digraph.vertices[e.source]
            #F.vertices[e.target] = digraph.vertices[e.target]
            F.add_edge(e)
            parent[e.target] = e.source

        for vw in digraph.vertices[v].out_edges:
            assert isinstance(vw, DiGraph.Edge)
            w = vw.target
            assert isinstance(w, str)

            if w in Q and vw.distance < C[w]:
                C[w] = vw.distance
                E[w] = vw

    #print('Start: {:}; End: {:}'.format(start, end))
    return extract_path(parent, end)