
def extract_path(parent, target):
    """
    Extracts a path through the network
    :param parent: a dictionary of parent vertices, keyed by vertex name
    :param target: the final vertex in the path
    :return: a list of vertices that should be visited to get from the source to the target
    """
    result = []

    #   We'll begin at the end, and work backward
    current = target
    while current is not None:
        #   Add the current vertex and visit its parent
        result.append(current)
        current = parent[current]

    #   Add the source vertex last, then reverse the order of the list before returning it
    result.reverse()
    return result