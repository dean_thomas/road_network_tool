import math
from algorithms.util import extract_path


def dijkstra(digraph, start, target):
    """
    Use dijkstra's algorithm to compute the shortest path between two vertices
    :param digraph: the digraph representing the network
    :param start: the starting vertex
    :param target: the intended target vertex
    :return: an array listing the vertices in order between the source and target
    """
    #   Dictionaries to keep track of the distance from the source and parent of each vertex
    dist = {}
    prev = {}

    #   List of vertices for processing
    queue = []

    #   Mark distance to all vertices as infinity and with no parent; add each vertex to the queue
    for vertex in digraph.vertices:
        dist[vertex] = math.inf
        prev[vertex] = None
        queue.append(vertex)

    # print_distance(digraph, distance)

    #   Mark the distance to the source as 0
    dist[start] = 0

    while len(queue) > 0:
        #   Find the nearest vertex (u) to the source (still in the queue)
        u = min(queue, key=lambda v: dist[v])

        #   Remove from the queue
        queue.remove(u)

        #   Loop through each of the unvisited neighbours of vertex u
        unvisited_neighbours = list(filter(lambda edge: edge.target in queue, digraph.vertices[u].out_edges))
        for e in unvisited_neighbours:
            neighbour = e.target

            #   Compute the distance to the neighbour
            alt = dist[u] + e.distance
            if alt < dist[neighbour]:
                #   If the computed distance is less that already found update it
                dist[neighbour] = alt
                prev[neighbour] = u

    # print_distance(digraph, dist)

    #   trace the path backward from the target
    return extract_path(prev, target)